﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    public static BoardManager Instance{set;get;}
    private bool [,] allowedMoves{set;get;}

    private const float TILE_SIZE=1.0f;
    private const float TILE_OFFSET=0.5f;

    private int selectionX=-1;
    private int selectionY=-1;

    public List<GameObject> chessmanPrefabs;
    private List<GameObject> activeChessman;


    private Quaternion orientation=Quaternion.Euler(0,180,0);


    //[Header("part2")]
    public Chessman[,] chessmans{set;get;}
    private Chessman selectedChessman;

    public bool isWhiteTurn=true;


    private Material previousMat;
    public Material selectedMat;


    public int[] EnPassantMove{set;get;}


    private void Start() {
        Instance=this;
        SpawnAllChessmans();
    }

    private void Update() {
        UpdateSelection();
        DrawChessBoard();
        if(Input.GetMouseButtonDown(0))
        {
            if(selectedChessman==null)      //不知道為何原影片這邊是if(selectionX>=0&&selectionY>=0)，不都會>0嗎
            {
                //Select the chessman
                SelectChessman(selectionX,selectionY);
            }
            else
            {
                MoveChessman(selectionX,selectionY);
            }
        }

    

        
    }



    private void DrawChessBoard()
    {
        Vector3 widthLine=Vector3.right*8;
        Vector3 heighLine=Vector3.forward*8;

        for(int i=0;i<=8;i++)
        {
            Vector3 start=Vector3.forward*i;
            Debug.DrawLine(start,start+widthLine);
            for(int j=0;j<=8;j++)
            {
                start=Vector3.right*i;
                Debug.DrawLine(start,start+heighLine);
            }
        }

        //Draw the selection
        if(selectionX>=0&&selectionY>=0)
        {
            Debug.DrawLine(Vector3.forward*selectionY+Vector3.right*selectionX,Vector3.forward*(selectionY+1)+Vector3.right*(selectionX+1),Color.black,2.0f,false);
            Debug.DrawLine(Vector3.forward*(selectionY+1)+Vector3.right*selectionX,Vector3.forward*selectionY+Vector3.right*(selectionX+1),Color.black,2.0f,false);
        }
    }

    private void UpdateSelection()     //射線偵測點選位置
    {
        if(!Camera.main)
        {
            return;
        }

        RaycastHit hit;
        if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition),out hit,25.0f,LayerMask.GetMask("ChessPlane")))
        {
            selectionX=(int)hit.point.x;
            selectionY=(int)hit.point.z;
        }
        else
        {
            selectionX=-1;
            selectionY=-1;
        }
    }

    private void SpawnWhiteChessman(int index,int x,int y)
    {
        GameObject go=Instantiate(chessmanPrefabs[index],GetTileCenter(x,y),Quaternion.identity)as GameObject;
        go.transform.SetParent(transform);

        chessmans[x,y]=go.GetComponent<Chessman>();
        chessmans[x,y].SetPosition(x,y);
        activeChessman.Add(go);
        
    }

    private void SpawnBlackChessman(int index,int x,int y)
    {
        GameObject go=Instantiate(chessmanPrefabs[index],GetTileCenter(x,y),orientation)as GameObject;
        go.transform.SetParent(transform);

        chessmans[x,y]=go.GetComponent<Chessman>();
        chessmans[x,y].SetPosition(x,y);
        activeChessman.Add(go);
        
    }


    private Vector3 GetTileCenter(int x,int y)
    {
        Vector3 origin=Vector3.zero;
        origin.x+=(TILE_SIZE*x)+TILE_OFFSET;
        origin.z+=(TILE_SIZE*y)+TILE_OFFSET;
        return origin;
    }

    private void SpawnAllChessmans()
    {
        activeChessman=new List<GameObject>();
        chessmans=new Chessman[8,8]; //初始位置陣列
        EnPassantMove=new int[2]{-1,-1};
        //Spawn the white team
        //King
        SpawnWhiteChessman(0,3,0);
        //Queen
        SpawnWhiteChessman(1,4,0);
        //Rooks
        SpawnWhiteChessman(2,0,0);
        SpawnWhiteChessman(2,7,0);
        //Bishops
        SpawnWhiteChessman(3,2,0);
        SpawnWhiteChessman(3,5,0);
        //Knights
        SpawnWhiteChessman(4,1,0);
        SpawnWhiteChessman(4,6,0);
        //Pawns
        for(int i=0;i<8;i++)
        {
            SpawnWhiteChessman(5,i,1);
        }



        SpawnBlackChessman(6,3,7);
        //Queen
        SpawnBlackChessman(7,4,7);
        //Rooks
        SpawnBlackChessman(8,0,7);
        SpawnBlackChessman(8,7,7);
        //Bishops
        SpawnBlackChessman(9,2,7);
        SpawnBlackChessman(9,5,7);
        //Knights
        SpawnBlackChessman(10,1,7);
        SpawnBlackChessman(10,6,7);
        //Pawns
        for(int i=0;i<8;i++)
        {
            SpawnBlackChessman(11,i,6);
        }
    }

    private void SelectChessman(int x,int y)     //選取棋子
    {
        if(chessmans[x,y]==null)
        {
            return;
        }

        if(chessmans[x,y].isWhite!=isWhiteTurn)
        {
            return;
        }
        bool hasAtleastOneMove=false;
        allowedMoves=chessmans[x,y].PossibleMove();
        for(int i=0;i<8;i++)
        {
            for(int j=0;j<8;j++)
            {
                if(allowedMoves[i,j]!=null)   
                    hasAtleastOneMove=true;
            }
        }
        if(!hasAtleastOneMove)
            return;

        selectedChessman=chessmans[x,y];

        previousMat=selectedChessman.GetComponent<MeshRenderer>().material;
        selectedMat.mainTexture=previousMat.mainTexture;
        selectedChessman.GetComponent<MeshRenderer>().material=selectedMat;

        BoardHighlights.Instance.HighlghtAllowedMoves(allowedMoves);
    }

    private void MoveChessman(int x,int y)    //移動
    {
        if(allowedMoves[x,y])
        {
            Chessman c=chessmans[x,y];
            if(c!=null&&c.isWhite!=isWhiteTurn)
            {
                //Capture a piece
                //If it is the king
                if(c.GetType()==typeof(King))
                {
                    //End the game
                    EndGame();
                    return;
                }

                activeChessman.Remove(c.gameObject);
                Destroy(c.gameObject);
            }

            if(x==EnPassantMove[0]&&y==EnPassantMove[1])
            {
                if(isWhiteTurn)
                {
                    c=chessmans[x,y-1];
                }
                else
                {
                    c=chessmans[x,y+1];
                }

                activeChessman.Remove(c.gameObject);
                Destroy(c.gameObject);
            }
            EnPassantMove[0]=-1;
            EnPassantMove[1]=-1;

            if(selectedChessman.GetType()==typeof(Pawn))
            {
                if(y==7)
                {
                    activeChessman.Remove(selectedChessman.gameObject);
                    Destroy(selectedChessman.gameObject);
                    SpawnWhiteChessman(1,x,y);
                }
                else if(y==0)
                {
                    activeChessman.Remove(selectedChessman.gameObject);
                    Destroy(selectedChessman.gameObject);
                    SpawnBlackChessman(7,x,y);
                }
                if(selectedChessman.CurrentY==1&&y==3)
                {
                    EnPassantMove[0]=x;
                    EnPassantMove[1]=y-1;
                    
                }
                else if(selectedChessman.CurrentY==6&&y==4)
                {
                    EnPassantMove[0]=x;
                    EnPassantMove[1]=y+1;
                }
            }
            chessmans[selectedChessman.CurrentX,selectedChessman.CurrentY]=null;
            selectedChessman.transform.position=GetTileCenter(x,y);
            selectedChessman.SetPosition(x,y);
            chessmans[x,y]=selectedChessman;
            isWhiteTurn=!isWhiteTurn;
        }
        selectedChessman.GetComponent<MeshRenderer>().material=previousMat;
        selectedChessman=null;
        BoardHighlights.Instance.HideHighlights();
        
    }


    private void EndGame()
    {
        if(isWhiteTurn)
        {
            Debug.Log("White team wins");
        }
        else
        {
            Debug.Log("Black team wins");
        }

        foreach(GameObject go in activeChessman)
        {
            Destroy(go);
        }
        isWhiteTurn=true;
        BoardHighlights.Instance.HideHighlights();
        SpawnAllChessmans();
    }





}



